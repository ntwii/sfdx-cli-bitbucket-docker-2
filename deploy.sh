#!/bin/bash

# get different changes between current commit and previous commit
git diff ${LAST_COMMIT_HASH} HEAD --name-status > change-files.txt

# generate changes.txt that stored file path need to deploy to Salesforce
node /read-file.js change-files.txt ${COMPLEX_COMPONENTS}

# list change files detect from git commit
cat change-files.txt

# deploy only changed files in current commit
if [ -f changes.txt ] 
then
    echo "Changed Files >> $(cat changes.txt)"
    sfdx force:source:deploy -u ${SFDX_ORG_USERNAME} --sourcepath "$(cat changes.txt)"
else
    echo "No changed file found in this commit"
fi

# delete files disappearing from current commit
if [ -f deletes.txt ] 
then
    echo "Deleted Files >> $(cat deletes.txt)"
#    sfdx force:source:delete -u ${SFDX_ORG_USERNAME} --sourcepath $(cat deletes.txt)
else
    echo "No deleted file found in this commit"
fi